# Cyclabile, a light augmented bike lane emitter

Cyclabile is an experiment about projecting a moving bike lane on the road: the
lane moves when the bike moves, producing the effect of riding on an actual bike
lane, even when there isn't one.

Dependencies:

* `libam7xxx0.1` - a library to use USB pico-projector based on the am7xxx chip.
* `libturbojpeg0` - a library for fast JPEG encoding
* `liblept5` - a library for image processing, used to calculate a perspective
  projection.

## Limitations

The effectiveness of projecting on the paving instead of signaling the presence
with a traditional light can be argued.

For now the project purpose is more about making a statement (there should be
more bike lanes) than to build an actual product.

## Instruction

The prototype has been developed on a BeagleBone Black.

A keypad is needed to adjust the perspective projection parameters, see
https://ao2.it/137 for instructions about how a device-tree overlay can be used
for that.

A precise rotary encoder is needed to detect movement at very low speed,
something like the one at https://ao2.it/135 can be used.

The instructions at https://ao2.it/138 show an example of how to build a support
to mount a magnet ring on the front wheel of the bike.

The device-tree overlay at https://git.ao2.it/experiments/bbb-eqep2b-ao2.git/
enables the BBB hardware support for rotary encoders.

To build the project for the BeagleBone Black execute the following command:

    $ make NEON=1 EQEP=1

And install the systemd unit to launch the program automatically when the USB
projector gets connected:

    $ sudo make enable_service

The software can also be tested on a normal desktop PC, emulating the encoder
with the mouse wheel, with the following command:

    $ make && sudo make run


## Similar projects

### Safety First

"*Safety First*" is an art installation by Vladimír Turner and Ondřej Mladý very
much in the spirit of Cyclabile.

* <http://sgnlr.com/works/outside/safety-first2011-prague/>
* <https://vimeo.com/23469752>

The difference is that Cyclabile is more portable and more interactive, the bike
lane moves depending on the bike movement while "*Safety First*" used a video
recording in a fixed loop.


### Bike Projector Headlight

This is more generic, it's used to display info on the road, not specifically
a bike lane.

* <https://makezine.com/bike-projector-heads-down-display-stationary/>
* <https://www.reddit.com/r/technology/comments/1anmip/guy_builds_a_speedometer_built_from_a_raspberry/>
* <https://www.youtube.com/watch?v=Nfk1-XMASrk>


### Lightlane

A laser bike lane, it projects a static image.

* <https://www.wired.com/2009/02/lightlanes-lase/>
