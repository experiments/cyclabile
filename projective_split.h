/* 
 * Separate calculating projected coordinates and transforming the pixels.
 *
 * Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROJECTIVE_SPLIT_H
#define PROJECTIVE_SPLIT_H

#include <leptonica/allheaders.h>

struct p {
	l_int32 x;
	l_int32 y;
};

struct p *
_pixProjectiveSampled_precalc_map(BOX        *viewport,
					l_float32  *vc);

PIX *
_pixProjectiveSampled_apply_map_dest_roi(PIX        *pixs,
			       struct p   *map,
			       l_int32     incolor,
			       PIX        *pixd,
			       BOX        *roi);

#ifdef USE_NEON
PIX *
_pixProjectiveSampled_apply_map_dest_roi_neon(PIX        *pixs,
			       struct p   *map,
			       l_int32     incolor,
			       PIX        *pixd,
			       BOX        *roi);
#endif

#endif /* PROJECTIVE_SPLIT_H */
