CFLAGS = -std=c99 -pedantic -pedantic-errors -Wall -g3 -O2 -D_ANSI_SOURCE_
CFLAGS += -fno-common \
	  -Wall \
	  -Wdeclaration-after-statement \
	  -Wextra \
	  -Wformat=2 \
	  -Winit-self \
	  -Winline \
	  -Wpacked \
	  -Wp,-D_FORTIFY_SOURCE=2 \
	  -Wpointer-arith \
	  -Wlarger-than-65500 \
	  -Wmissing-declarations \
	  -Wmissing-format-attribute \
	  -Wmissing-noreturn \
	  -Wmissing-prototypes \
	  -Wnested-externs \
	  -Wold-style-definition \
	  -Wredundant-decls \
	  -Wsign-compare \
	  -Wstrict-aliasing=2 \
	  -Wstrict-prototypes \
	  -Wundef \
	  -Wunreachable-code \
	  -Wunsafe-loop-optimizations \
	  -Wunused-but-set-variable \
	  -Wwrite-strings

LDLIBS = -llept -lturbojpeg -lam7xxx

# For clock_nanosleep()
CFLAGS += -D_POSIX_C_SOURCE=200112L

# for clock_gettime()
LDLIBS += -lrt

# Some compiler optimizations
CFLAGS += -O3 \
	    -fno-strict-aliasing \
	    -ftree-vectorize \
	    -ffast-math \
	    -funroll-loops \
	    -funsafe-math-optimizations \
	    -fsingle-precision-constant

# NEON optimizations
ifeq ($(NEON), 1)
  CFLAGS += -march=armv7-a \
	    -mtune=cortex-a8 \
	    -mfpu=neon \
	    -mfloat-abi=hard \
	    -DUSE_NEON
endif

# Use the BBB eQEP unit
ifeq ($(EQEP), 1)
  CFLAGS += -DUSE_EQEP
endif


cyclabile: cyclabile.o projective_split.o

clean:
	rm -rf *~ *.o cyclabile cyclabile.service

run:
	./cyclabile -P 2 -f images/bike_lane.png -e /dev/input/event1

install_service:
	cp 90-projector.rules $(DESTDIR)/lib/udev/rules.d/
	udevadm control --reload
	sed -e 's!@@CYCLABILE_SOURCE_PATH@@!$(shell pwd)!' < cyclabile.service.in > cyclabile.service
	cp cyclabile.service $(DESTIR)/lib/systemd/system/

uninstall_service: disable_service
	rm $(DESTIR)/lib/systemd/system/cyclabile.service
	rm $(DESTDIR)/lib/udev/rules.d/90-projector.rules
	udevadm control --reload

disable_service:
	systemctl disable cyclabile.service

enable_service: install_service
	systemctl enable cyclabile.service

test: cyclabile
	valgrind --suppressions=contrib/libusb-udev.supp \
	--leak-check=full --show-reachable=yes \
	./cyclabile -f images/bumpy_road.png -e /dev/input/event1
